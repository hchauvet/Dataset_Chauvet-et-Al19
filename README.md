# Dataset_Chauvet-et-Al19

Repository for dataset and notebook used in article "Revealing the hierarchy of processes and time scales that control the tropic response of shoots to gravi-stimulations" submited to JexpBot.

You can interact directly with the data and python notebook using Binder! (just clic on the badge below)

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fframagit.org%2Fhchauvet%2FDataset_Chauvet-et-Al19/62e33bc0963005fb1483788aa525e92563a6fd16?filepath=Description.ipynb)

# Datasets from experiments on wheat coleoptile. 

Results from these datasets have been submited in JexpBot in the paper "Reavealing the hierarchy of processes and time scales that control the tropic response of shoots to gravi-stimulations". 

This archive provides the data extracted from experiments time-lapse images and python notebooks to compute the figures of the published paper.

## Figure 5
Gravitropic response to transient stimulus compared to a theoretical
model.

See file [Figure5.ipynb](./Figure5.ipynb) for detailed instructions on reading and
using data.

Data in the folder "./data/Figure5/":

- The data file "permanent_stimulus_curves_1g.txt" contains time
  evolution of the length and tip angle of wheat coleoptile inclined
  at 45° under 1g.  The data are formatted as follow:
    
    - T_varTTx: the time in minute for the experiment n°x.
    - T_varAx: time evolution of the cooleoptile tip angle (in degree) 
      for the experiment n°x.
    - T_varLx: time evolution of the coleoptile length (in millimetre) 
      for the experiment n°x.


- The data file "transient_mean_curves_1g.hdf" is in HDF format and
  contains time evolution of the tip angle for transient inclination
  with several duration (inclination angle:45°, gravity intensity:1g).
  The data in the file are registered in the store named "data":
  
    - columns: [3,5,8,11,15,20,25,30] give the inclination duration in
      minute.
    - angles: the time evolution of the coleoptile tip angle (in degree).
    - temps: the time (in minute).


- The data file "gravitropic_response_1g.csv" is in csv format and
  contains data to compute the gravitropic sensitivity for transient
  experiments (inclination angle:45°, gravity intensity:1g).
  
    - manip: is the date of the experiment.
    - sequence: is the number of repetition for the same day of manip.
    - temps_expo_(min): is the transient inclination time in minute.
    - Rayon_(pixel): is the mean radius of the coleoptile in pixel.
    - DADT_moy_(deg/min): is the angle velocity in degree/minute.
    - DLDT_moy_(pix/min): is the growth rate in pixel/minute.
    - Reponse_moyenne_(deg): is the gravitropic response computed from
      columns Rayon_(pixel)*DADT_moy_(deg/min)/DLDT_moy_(pix/min).
    - conversion length scale: 50.8 pixel/cm.


- The data file "gravitropic_response_3g.csv" is in csv format and
  contains data to compute the gravitropic sensitivity for transient
  experiments (inclination angle:45°, gravity intensity:3g).
  
    - manip: is the date of the experiment.
    - expo: is the transient inclination time in minute.
    - dadt_(deg/min): is the angle velocity in degree/minute.
    - dldt_(pix/min): is the growth rate in pixel/minute.
    - conversion length scale: 52.8 pixel/cm.
    
    
## Figure 6
Dynamic of the statoliths pile avalanche for different gravity
intensity and different inclination angle.

See file [Figure6.ipynb](./Figure6.ipynb) for detailed instructions on reading and
using data.

Data in the folder "./data/Figure6/", The 3 datasets are contains:
    - time_(mn): the time in minute.
    - mean_angle_(deg): the statoliths pile mean 
      free surface angle in degree.
    - std_angle_(deg): the standard deviation for 
      the free surface angle in degree.

- The data file "Statolith_avalanche_dynamic_1g_70deg.csv" is in csv
  format and contains data for the statoliths pile avalanche time
  evolution for 1g and an inclination angle of 70°.

- The data file "Statolith_avalanche_dynamic_1g_45deg.csv" is in csv
  format and contains data for the statoliths pile avalanche time
  evolution for 1g and an inclination angle of 45°.

- The data file "Statolith_avalanche_dynamic_3g_70deg.csv" is in csv
  format and contains data for the statoliths pile avalanche time
  evolution for 3g and an inclination angle of 70°.


## Figure 7
Comparison of statoliths pile avalanche dynamic and the gravitropic
response to transient stimulus.

See file [Figure7.ipynb](./Figure7.ipynb) for detailed instructions on reading and
using data.

Data in the folder "./data/Figure7/":

- The data files "Statolith_avalanche_dynamic_1g_70deg.csv" and
  "Statolith_avalanche_dynamic_3g_70deg.csv" are the same than the
  ones on the **Figure 6**.

- The data files "gravitropic_response_1g.csv" and
  "gravitropic_response_3g.csv" are the same than the ones on the
  **Figure 5**.

- The data file "data_model_transient.csv" is in csv format and
  contains the data from the theoritical model calibrated on the
  **Figure 5**.
  
  - Beta: gravitropic sensitivity.
  - DT: transient inclination time in minute.
  
  
## Figure 8
Theoretical gravitropic sensitivity from the theoretical model for
different value of gravity intensity.

See file [Figure8.ipynb](./Figure8.ipynb) for the computation of the figure.